import React, {Component} from 'react';

export default class MenuItem extends Component{
    constructor(props){
      super(props);
      this.state = {
        hover:false,
      }
    }
    
    handleHover(){
      this.setState({hover:!this.state.hover});
    }
    
    render(){
      return(
        <div>
          <div 
            onMouseEnter={()=>{this.handleHover();}} 
            onMouseLeave={()=>{this.handleHover();}}
            onClick={this.props.onClick}>
            <div className="menu-list">
              <div className="menu-list-item">
                <div className="row">
                  <div className="menu-list-img">
                    <img src={'/static/img-error.png'}></img>
                  </div>
                  <div className="menu-list-description">
                    <h1>Smart TV Samsung Série 4 UN32J4300AG 32 polegadas LED Plana</h1>
                    <h2>10x R$ 134,11</h2>
                    <h2>R$ 1.139,90 à vista</h2>
                  </div>
                </div>
                <div className="close">X</div>
              </div>
            </div> 
          </div>
          <style jsx>
          {`
            hr {
              border-top: 1px solid #000;
            }
            .close {
              position: absolute;
              top: 15px;
              right: 15px;
            }
            .menu-list-item {
              background: #c49d00;
              margin: 2px 0;
              padding: 5px 15px;
              position: relative;
            }
            .row {
              display: flex;
              align-items:center;
            }
            .menu-list-item .menu-list-img {
              background: #fff;
              border: 1px solid #000;
              min-width: 80px;
              min-height: 80px;
            }
            .menu-list-item .menu-list-img img{
              width: 100%;
              height: auto;
            }
            .menu-list-item .menu-list-description {
              margin-left: 15px;
            }
            .menu-list-item .menu-list-description h1{
              color: #fff;
              font-size: 16px;
            }
            .menu-list-item .menu-list-description h2, .menu-list-item .resume h2{
              color: #000;
              font-size: 16px;
            }
            .menu-list-item .resume h2{
              text-align: right;
            }
          `}
          </style>
      </div>  
      )
    }
  }