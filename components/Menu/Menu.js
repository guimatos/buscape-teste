import React, { Component } from 'react'

  /* Menu.jsx */
  export default class Menu extends Component {
    constructor(props){
      super(props);
      this.state={
        open: this.props.open? this.props.open:false,
      }
    }
      
    componentWillReceiveProps(nextProps){
      if(nextProps.open !== this.state.open){
        this.setState({open:nextProps.open});
      }
    }
    
    render(){
      return(
        <div className="menu-list">
          {
            this.state.open?
              <div>
                {this.props.children}
              </div>:null
          }
          <style jsx>
          {`
            .menu-list {
                position: absolute;
                top: 180px;
                left: 0px;
                width: 100%;
                display: flex;
                flex-direction: column;
                background: #daaf00;
                transition: height 0.3s ease 0s;
                z-index: 2;
            }
            @media (max-width: 768px) {
              .menu-list{
                top: 108px;
              }
            }
          `}</style>
        </div>
      )
    }
  }
  