import React, { Component } from 'react'

  /* MenuButton.jsx */
  export default class MenuButton extends Component {
    constructor(props){
      super(props);
      this.state={
        open: this.props.open? this.props.open:false,
        color: this.props.color? this.props.color:'black',
      }
    }
  
    componentWillReceiveProps(nextProps){
      if(nextProps.open !== this.state.open){
        this.setState({open:nextProps.open});
      }
    }
    
    handleClick(){
    this.setState({open:!this.state.open});
    }
    
    render(){
      return(
        <div>
          <div className="menu-btn"
            onClick={this.props.onClick ? this.props.onClick: 
            ()=> {this.handleClick();}}>
            <img src={"/static/menu.png"}></img>
            <div className="counter">2</div>
          </div>
          <style jsx>
            {`
              .menu-btn {
                float: right;
                position: relative;
              }
              .menu-btn .counter {
                position: absolute;
                background-color: red;
                padding: 20px;
                color: #fff;
                width: 20px;
                height: 20px;
                text-align: center;
                border-radius: 50% 50%;
                bottom: -10px;
                left: -30px;
                font-size: 20px;
                font-weight: 500;
              }
            `}
          </style> 
        </div>
      )
    }
  }