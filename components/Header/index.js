import React, {Component} from 'react';
import Menu from '../Menu/Menu';
import MenuButton from '../Menu/MenuButton';
import MenuItem from '../Menu/MenuItem';

export default class Header extends Component {

  state = {
    menuOpen: false
  }

    handleMenuClick() {
      this.setState({menuOpen:!this.state.menuOpen});
    }

    handleLinkClick() {
        // this.setState({menuOpen: false});
    }
    render() {
        const menu = ['About Us','Our Products','Services','FAQ','Contact Us']
        const menuItems = menu.map((val,index)=>{
          return (
            <MenuItem 
              key={index} 
              delay={`${index * 0.1}s`}
              >{val}</MenuItem>)
        });

        return (
            <div>
                <div className="menu">
                  <div className="menu-container">
                    <div className="logo">
                      <img src={"/static/logo-buscape.png"}></img>
                    </div>
                    <MenuButton open={this.state.menuOpen} onClick={()=>this.handleMenuClick()} color='white'/>
                  </div>
                  <Menu open={this.state.menuOpen}>
                    {/* {menuItems} */}
                    <MenuItem></MenuItem>
                      <div className="resume">
                        <h2>subtotal</h2>
                        <hr></hr>
                        <h2>10x R$ 433,89</h2>
                        <h2>ou R$ 4.338,90 à vista</h2>
                      </div>
                  </Menu>
                </div>
                <style jsx>
                  {`
                    hr {
                      border-top: 1px solid #000;
                    }
                    .menu {
                      background: #ffcd00;
                      margin-bottom: 50px;
                      padding: 20px 15px;
                    }
                    .menu-container {
                      display:flex;
                      align-items:center;
                      width: 100%;
                      color: white;
                      justify-content: space-between;
                    }
                    .logo {
                      float: left;
                    }
                    .logo img {
                      max-width: 100%;
                      height: auto;
                    }
                    .menu-btn {
                      float: right;
                    }
                    .resume {
                      padding: 15px;
                    }
                   .resume h2{
                      color: #000;
                      font-size: 16px;
                    }
                    .resume h2{
                      text-align: right;
                    }
                    @media (max-width: 768px) {
                      .logo {
                        width: 100px;
                      }
                      .menu-btn img{
                        width: 60px;
                      }
                    }
                  `}
                </style>  
            </div>
        );
    }
}