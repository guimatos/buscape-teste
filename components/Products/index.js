import React, {Component} from 'react';

export default class Products extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            cart: JSON.parse(localStorage.getItem('cart')) || []
        }
    }

    addProductToCart(product){
        let cart = JSON.parse(localStorage.getItem('cart')) || [];
        cart.push(product);
        cart = JSON.stringify(cart);
        localStorage.setItem('cart', cart);   
    }

    changeImage(image){
        console.log(image);
        // console.log(this.state.items);
    }

    render() {
        const { product } = this.props;
        console.log(product);
        return (
            <div className="product">
                <div className="product-img">
                    <div className="product-img-list">
                        {this.props.item.product.images.map((img) => (
                            <div className="product-img-list-item"
                                onClick={() => this.changeImage({img})}>
                                <img src={img}
                                     onError={(e)=>{e.target.onerror = null; e.target.src="/static/img-error.png"}}></img>
                            </div>
                        ))}
                    </div>
                    <div className="product-img-show">
                        <img src={this.props.item.product.images[0]} style={{flex:1, height: undefined, width: undefined}}></img>  
                    </div>
                </div>
                <div className="product-description">
                    <div className="title">
                        <h1 key={this.props.item.product.id}>{this.props.item.product.name}</h1>
                        {/* <span>0</span> */}
                    </div>
                    <hr></hr>
                    {/* <div className="best-price">
                        <span>Melhor preço</span>
                    </div> */}
                    <div className="details">
                        <div className="price">
                            <div className="installments highligth">
                                <span className="text">{this.props.item.product.price.installments}X </span>
                                <span className="dolar">R$ </span>
                                <span className="price">{this.props.item.product.price.installmentValue}</span>
                            </div>
                            <div className="in-cash">
                                <span>ou </span>
                                <span className="price highligth">R$ {this.props.item.product.price.value}</span>
                                <span> à vista</span>
                            </div>
                        </div>
                        <div className="add-cart">
                            <span className="btn"
                                  onClick={() => this.addProductToCart(this.props.item)}>
                                    Adicionar ao carrinho >
                            </span>
                        </div>
                    </div>
                </div>
                <style jsx>
                    {`
                        hr {
                            border-top: 1px solid #e6e6e6;
                        }
                        img {
                            height: auto;
                            max-width: 100%;
                        }
                        .highligth {
                            color: #22b14c;
                        }

                        .btn {
                            background: #22b14c;
                            color: #fff;
                            padding: 15px 5px;
                            text-align: center;
                            display: block;
                            border-radius: 5px;
                            cursor: pointer;
                        }
                        .btn:hover {
                            background: red;
                            transition: 1s;
                        }
                        .product {
                            display: flex;
                            max-height: 400px;
                            margin: 15px 10px;
                            background: #fff;
                            border-radius: 4px;
                            box-shadow: 0 2px 4px 0 rgba(0,0,0,.06);
                            overflow: hidden;
                            max-width: 1000px;
                            margin: 30px auto;
                        }
                        .product-img {
                            width: 50%;
                            display: flex;
                            padding: 0 15px;
                            align-items: center;
                        }
                        .product-img-list {
                            width: 20%;
                            max-heigth: 280px;
                        }
                        .product-img-list-item {
                            height: 50px;
                            width: 50px;
                            border: 2px solid #ffdc4d;
                            border-radius: 5px;
                            margin-bottom: 5px;
                            display: flex;
                            align-items: center;
                            margin-bottom: 15px;
                        }
                        .product-img-show {
                            width: 80%;
                            text-align: center;
                            padding: 15px 30px;
                        }
                        .product-description {
                            width: 50%;
                            padding: 0 15px;
                        }
                        .product-description .title h1 {
                            font-size: 26px;
                            font-weight: 300;
                        }
                        .product-description .details {
                            display: flex;
                            margin-top: 60px;
                        }
                        .product-description .details .price, .product-description .details .add-cart {
                            width: 50%;
                        }
                        .product-description .details .price .installments {
                            font-weight: 500;
                        }
                        .product-description .details .price .installments .text {
                            font-size: 16px;
                        }
                        .product-description .details .price .installments .dolar {
                            font-size: 20px;
                        }
                        .product-description .details .price .installments .price {
                            font-size: 40px;
                        }
                        .product-description .details .price .in-cash  {
                            color: #757575;
                            font-weight: 500;
                        }
                        .product-description .details .price .in-cash .price  {
                            font-size: 18px;
                        }

                        @media (max-width: 768px) {
                            .product {
                                padding: 15px 0;
                                display: block;
                                max-height: unset;
                            }
                            .product-img, .product-description {
                                width: unset;
                            }
                            .product-description .details .price, .product-description .details .add-cart {
                                width: 100%;
                            }
                            .product-description .details {
                                display: block;
                                margin-top: 0;
                            }
                            .product-description .details .price {
                                text-align: center;
                                margin-bottom: 15px;
                            }
                        }

                    `}
                </style>
            </div>
        );
    }
}