import React, {Component} from 'react';
import axios from 'axios';
import Link from 'next/link';
import Header from '../components/Header/';
import Products from '../components/Products';

export default class Home extends Component {

    state = {
        items: [],
        menuOpen: false
    }

    componentDidMount() {
        axios.get("/static/data.json").then(res => {
            const items = res.data.items;
            this.setState({ items });
        })
    } 

    render() {
        return (
            <div>
                <Header />
                {this.state.items.map((item) => (
                    <Products item={item} />
                ))}
                <style jsx global>
                    {`
                        @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,500');

                        body {
                            background: #f3f3f3;
                            font-family: 'Montserrat', sans-serif;
                            margin: 0;
                        }
                    `}
                </style>
            </div>
        );
    }
}